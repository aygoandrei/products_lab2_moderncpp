#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(const int32_t& id, const std::string& name, const float& rawPrice, NonperishableProductType& type) :
	Product(id, name, rawPrice), m_type(type)
{
	// empty
}

NonperishableProductType NonperishableProduct::GetType() const
{
	return this->m_type;
}

int32_t NonperishableProduct::GetVAT() const
{
	return this->m_VAT;
}

float NonperishableProduct::GetPrice() const
{
	return static_cast<float>(this->m_rawPrice + this->m_VAT * 0.01 * this->m_rawPrice);
}
